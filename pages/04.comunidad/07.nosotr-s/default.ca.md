---
title: 'SOBRE NOSOLTRES'
---

**Anartist**, com a projecte, va néixer el 2019, quan vam començar a instal·lar i utilitzar eines de programari lliure, en un procés d'emancipació dels nostres projectes artístics i personals de la _gran indústria tecnològica_. Poc a poc, vam anar creant una **comunidad virtual**; vam crear el [fòrum](https://forum.anartist.org) el març de 2020 i des de llavors la comunitat no ha parat de créixer. 

La idea de crear una comunitat virtual com alternativa als gegants tecnològics va sorgir de la ètica al mateix temps que de la necessitat. Creiem que l'internet ha revolucionat totalment la forma de compartir la cultura i el context dels projectes artístics actuals. Per una banda, ens ha alliberat de molts intermediaris, fent possible la creació lliure de les **nostres pròpies xarxes de suport**. Però per altra banda, han apregut intermediaris nous molts més grans, forts i internacionals que els que teniem abans: les empreses més grans del món. Aquestes empreses imposen el seu joc i les seves normes, com el finançament a través de la _publicitat_ o el _control social_ implícit en tota centralització.

Veiem la necessitat de tornar a lo petit, a lo local, a la comunitat, al mateix temps que construim xarxes federades que ens permetin connectar amb altres comunitats de tot el món. Volem **descentralitzar l'art i la cultura** per descentralitzar el món. Volem trobar noves formes més ètiques, justes i lliures d'autogestionar o finançar projectes artístics, de compartir l'art i la cultura i d'entendre l'internet i la tecnologia.

En quant a la praxis d'Anartist, estem construint el projecte de forma comunitària a través del nostre [fòrum](https://forum.anartist.org). Per ara, no hi ha ningú cobrant i tota la comunitat es gestiona **de forma voluntària**. Compartim els gastos del lloguer del servidor que sostenta les nostres eines a través de la [lliure donació i les quotes](https://opencollective.com/anartist). Entre les discussions del fòrum i les assemblees anem perfilant el futur de la comunitat.

Portem un nou món a les nostres eines.

Anartist.

