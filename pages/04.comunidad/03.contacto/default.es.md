---
title: CONTACTAR
---

# Para contactar con la plataforma:
**Mail**: info[at]anartist.org  
**Foro**: [forum.anartist.org](https://forum.anartist.org)  
**Mastodon**: [@anartist@social.anartist.org](https://social.anartist.org/web/accounts/1)