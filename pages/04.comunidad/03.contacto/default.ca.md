---
title: CONTACTAR
---

# Per contactar amb Anartist:
**Mail**: info[at]anartist.org  
**Fòrum**: [forum.anartist.org](https://forum.anartist.org)  
**Mastodon**: [@anartist@social.anartist.org](https://social.anartist.org/web/accounts/1)