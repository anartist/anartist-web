---
title: CONTRIBUIR
---

<h3 align=center> QUIERES CONTRIBUIR? </h3>
Puedes colaborar con la comunidad redactando textos, diseñando la web, creando un logo o cualquier cosa que creas que puedas aportar. Todo apoyo es bienvenido!  
También puedes contribuir económicamente mediante **OPEN COLLECTIVE**
<script src="https://opencollective.com/anartist/contribute/button.js" color="white"></script>  
Open Collective es nuestra herramienta principal para compartir los gastos de la comunidad. Allí encontrarás el presupuesto anual y los distintos tipos de cuotas y el coste de cada una de ellas.  

<iframe src="https://social.anartist.org/@anartist/109337651073658492/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="1500" allowfullscreen="allowfullscreen"></iframe><script src="https://social.anartist.org/embed.js" async="async"></script>
