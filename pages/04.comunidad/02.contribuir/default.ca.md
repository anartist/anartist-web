---
title: CONTRIBUIR
---

<h3 align=center> VOLS CONTRIBUIR? </h3>
Pots col·laborar amb la comunitat redactant textos, dissenyant la web, creant un logo o fent qualsevol cosa que creguis que pot aportar. Tot suport és benvingut!
També pots contribuir econòmicament a través de **OPEN COLLECTIVE**
<script src="https://opencollective.com/anartist/contribute/button.js" color="white"></script>  
Open Collective és la nostra eina principal per compartir els gastos de la comunitat. Allà hi trobaràs el pressupost anual i els diferents tipus de quotes i el cost de cada una d'elles.  

<iframe src="https://social.anartist.org/@anartist/109337651073658492/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="1500" allowfullscreen="allowfullscreen"></iframe><script src="https://social.anartist.org/embed.js" async="async"></script>
