---
title: 'Cómo migrar de un servidor Mastodon a otro'
---

## Cómo migrar de un servidor Mastodon a otro
> Mastodon es el software bajo la herramienta Social

Cómo cambiar de servidor y mover _la mayoría_ nuestros datos a una cuenta de Mastodon nueva.

1. Crea una cuenta en el nuevo servidor.
2. En el **nuevo** servidor, ir a Preferencias -> Cuenta -> Migrar DE una cuenta Diferente.
3. Poner el alias (nombre) del servidor ANTIGUO.
4. En el servidor ANTIGUO ir a Cuenta -> Migrar A otra cuenta.
5. Poner el alias (nombre) de la cuenta NUEVA y enviar.

