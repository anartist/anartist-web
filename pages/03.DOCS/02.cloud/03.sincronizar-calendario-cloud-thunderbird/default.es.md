---
title: 'Sincronizar el calendario de Cloud con Thunderbird'
media_order: 'sinc1.png,sinc2.png,sinc3.png,sinc4.png'
---

## Sincronizar el calendario de Cloud con Thunderbird
Una de las herramientas que podemos usar en escritorio para el manejo del calendario de Cloud es el complemento Lightning Thunderbird

Copiamos el enlace privado del calendario a sincronizar  
![](sinc1.png)

Creamos un nuevo calendario pulsando el botón derecho del ratón sobre el panel de calendarios a la izquierda de Lightning y seleccionamos “En la red”  
![](sinc2.png)

Seleccionamos Formato “CalDav” e introducimos nuestro nombre de usuaria en Cloud y el enlace que hemos copiado anteriormente  
![](sinc3.png)

Nos solicitará la contraseña y posiblemente debamos seleccionar que la gestione con el gestor de contraseñas para evitar que en cada reinicio nos la solicite

Le damos nombre y color al calendario  
![](sinc4.png)
