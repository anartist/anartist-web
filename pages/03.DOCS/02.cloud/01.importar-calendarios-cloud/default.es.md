---
title: 'Importar calendarios a Cloud'
media_order: 'importar1.png,importar2.png,importar3.png'
---

## Importar calendarios a Cloud
En nextcloud podemos importar calendarios en formato ics, formato de archivo ampliamente usado en calendarios.

![](importar1.png)

Desplegamos el menú “Ajustes & importar” de la parte inferior de la herramienta Calendario.

Pulsamos Import calendar

![](importar2.png)

Seleccionamos el archivo ICS a importar

![](importar3.png)

Podemos seleccionar un calendario ya creado e importar los datos o bien crear un nuevo calendario.

La importación no es instantánea, irán apareciendo los eventos en los siguientes minutos.