---
title: 'Sincronizar el calendario de Cloud con Android'
media_order: 'sinc1.png,sinc2.png,sinc3.png,sinc4.png'
---

# Sincronizar el calendario de Cloud con Android
[TOC]
Para la sincronización del calendario con Android será necesaria la instalación de una aplicación que gestione la sincronización con el dispositivo. Una vez instalada se pueden utilizar las diferentes aplicaciones de calendario.  
La aplicación que actualmente (2021) mejor funciona en Android es DAVx5. Se puede encontrar en el repositorio de aplicaciones F-Droid (gratis) y en PlayStore (bajo tarifa).  
La configuración se puede realizar desde la aplicación de Nextcloud para Android o bien directamente desde DAVx5

## Configuración desde la aplicación de Nextcloud
En la configuración de la aplicación de Nextcloud para Android pulsamos sobre “Sincronizar calendario y contactos”. Nos redirigire a la aplicación DAVx5  
![](andro1.png)

Accedemos con las credenciales de Cloud  
![](andro2.png)  ![](andro3.png)  ![](andro4.png)

Aceptamos crear cuenta con nuestro mail como nombre  
![](andro5.png)

Marcamos el o los calendarios importados y pulsamos sobre el boton inferior de refrescar para cargar el calendario en el dispositivo.  
![](andro6.png)  
La sincronización no es instantánea, irán apareciendo los eventos en los siguientes minutos

## Configuración desde DAVx5
1. Entramos en DAVx5 y pulsamos en el botón “+” para añadir una cuenta.
2. Seleccionamos “Acceder con URL y nombre de usuario”  
![](andro7.png)  
3. La URL es la siguiente: [https://cloud.anartist.org/remote.php/dav](https://cloud.anartist.org/remote.php/dav)
4. Introducimos las credenciales de Cloud
5. Los siguientes pasos son iguales a los de la configuración anterior
6. En los ajustes de DAVx5 podemos cambiar los tiempos y condiciones de la sincronización