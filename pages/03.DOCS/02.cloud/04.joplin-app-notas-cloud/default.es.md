---
title: 'Joplin, app de notas sincronizada con Cloud'
media_order: 'Screenshot_20200712_100812.png,Screenshot_20200712_104135.png,Screenshot_20200712_104234.png'
---

# Joplin, app de notas avanzada sincronizada con Cloud
[TOC]
Cloud tiene una app de notas pero es algo sencilla, si hacemos un uso extendido de  este tipo de aplicaciones, Joplin nos va a dar características de lo más avanzadas.  
* Organización de las notas por libretas
* Creación de etiquetas para una organización y búsqueda rápida
* Escritura en Markdown
* Escritura con WYSIWYG (beta)
* Inserción de imágenes y archivos
* Importación desde Evernote
* Sincronización con Nextcloud y otras nubes privativas
* Extensión en exploradores web (firefox, chrome) con opción de guardar páginas web, selección de textos o capturas de pantalla

## Instalación
Podemos encontrar la aplicación en los centros de software de muchas distribuciones de GNU/Linux. Para Android, con APK descargable o en Google Play. Más información en [https://joplinapp.org/](https://joplinapp.org/)

## Sincronización con Cloud de Anartist
En primer lugar tendremos que **crear la carpeta “Joplin”** en nuestro directorio raíz de Cloud.

### Aplicación escritorio GNU/Linux
![](Screenshot_20200712_100812.png)
1. Nos dirigimos a Herramientas/Opciones/Sincronización
2. Seleccionamos Nextcloud en “Destino de sincronización”
3. Pegamos la dirección de nuestro servidor WebDav
**https://cloud.anartist.org/remote.php/webdav/Joplin**
4. Introducimos Usuario y contraseña
5. Pulsamos Comprobar sincronización

#### Complemento Joplin Web Clipper para navegadores web de escritorio
1. En la aplicación de escritorio Joplin nos dirigimos a **Herramientas>Opciones>WebCipper**  
![](Screenshot_20200712_104234.png)  
2. Habilitamos el “**servicio de recorte web**”

### Aplicación Android
1. Nos dirigimos a Configuración/Sincronización
2. Seleccionamos Nextcloud en “Destino de sincronización”
3. Pegamos la dirección de nuestro servidor WebDav
**https://cloud.anartist.org/remote.php/webdav/Joplin**
4. Introducimos Usuario y contraseña
5. Pulsamos Comprobar sincronización
6. Instalación extensión explorador web


