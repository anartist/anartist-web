---
title: 'Usar Anartist desde Android'
---

# Instalar las herramientas de Anartist en tu Android
[TOC]
Verás que para usar la gran mayoria de las herramientas que compartimos en Anartist, se accede a través del navegador de tu ordenador, siguiendo los enlaces de cada una de ellas (social.anartist.org, cloud.anartist.org, etc.). De todas formas, muchas veces queremos usar la misma herramienta en el móvil pero acceder desde su navegador no es nada cómodo. En este artículo, te recomendaremos algunas apps imprescindibles para usar las herramientas en tu Android.


## Conocimientos previos
Antes de empezar, es importante entender una cosa que nos servirá para instalar cualquier herramienta. Nos han acostumbrado a descargar una app como por ejemplo Twitter o Instagram, y que nos pida iniciar sesión, es decir, una cuenta y una contraseña. En el mundo descentralizado tenemos que añadirle otro campo antes de la cuenta y la contraseña: el servidor desde el que te quieres conectar. Es en este campo dónde tendremos que llenarlo con el enlace de la herramienta de Anartist que quieras usar: social.anartist.org, cloud, anartist.org o la que sea (-----.anartist.org). 

### F-Droid
Antes de empezar a instalar las apps, es importante pensar desde dónde queremos instalarlas. Sí que es verdad que muchas de las aplicaciones que leerás a continuación están en 'Google Play', pero algunas son de pago y todo lo que sea emanciparse de Google cumple nuestro ideal de descentralización. Por eso recomendamos F-Droid, un respositorio de aplicaciones libres donde encontrarás una variedad importante de apps sin publicidad y de código abierto. Para descargar F-Droid tienes que ir a su web y descargarlo directamente en tu Android: https://f-droid.org

## 1. Cloud (Nextcloud)
Al ser una herramienta multi-aplicaciones, puedes descargar distintas apps según cuál quieras usar:  
**Archivos:** [**Nextcloud**](https://f-droid.org/en/packages/com.nextcloud.client/)  
**Notas:** [**Nextcloud Notes**](https://f-droid.org/en/packages/it.niedermann.owncloud.notes/) / Ver: [Joplin, app de notas avanzada sincronizada con Cloud](https://anartist.org/es/docs/cloud/joplin-app-notas-cloud)  
**Deck:** [**Nextcloud Deck**](https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/)  
**Talk:** [**Nextcloud Talk**](https://f-droid.org/en/packages/com.nextcloud.talk2/)  
**Calendario:** [**Sincronizar el calendario de Cloud con Android**](https://anartist.org/es/docs/cloud/sincronizar-calendario-cloud-thunderbird)  
**Contactos:**

> Una vez instaladas te pedirán que inicies sesión:
> - Servidor: cloud.anartist.org
> - Cuenta: tu_usuaria
> - Contraseña: ············

## 2. Mail
Te funciona con cualquier app/cliente de mail. Algunas recomendaciones son:  
Apps: [**K-9 Mail**](https://f-droid.org/en/packages/com.fsck.k9/) / [**FairEmail**](https://f-droid.org/en/packages/eu.faircode.email/) / [**LibremMail**](https://f-droid.org/en/packages/one.librem.mail/)  
> Aquí no te pedirán el servidor y normalmente se sincronizará de forma automática. Tendrás que entrar:
> - Cuenta: ejemplo@anartist.org
> - Contraseña: ················  
> Si tienes algun problema se sincornización y te pide el IMAP, SMTP o POP3, ver: [Configuración correo Anartist](https://anartist.org/es/docs/mail/configuracion-correo-anartist)  

## 3. Social (Mastodon)
App: [**Tusky**](https://f-droid.org/en/packages/com.keylesspalace.tusky/) / [**Fedilab**](https://f-droid.org/en/packages/fr.gouv.etalab.mastodon/)  
> Una vez instalada te pedirán que inicies sesión:
> - Servidor / instancia: social.anartist.org
> - Mail: tu_usuaria@anartist.org
> - Contraseña: ············

## 4. Video (Peertube)
App: [**NewPipe**](https://f-droid.org/en/packages/org.schabi.newpipe/)  
> Una vez instalada podrás ver el contenido de Anartist si vas a:  
> *Menú> Ajustes > Contenido > Instancias de Peertube*  
> Y allí añades una nueva instancia: **video.anartist.org**

## 5. Pixelfed
Apps: [**Fedilab**](https://f-droid.org/en/packages/fr.gouv.etalab.mastodon/) / [**PixelDroid**](https://apt.izzysoft.de/fdroid/repo/com.h.pixeldroid_10.apk)  
> Una vez instalada te pedirán que inicies sesión:
> - Servidor / instancia: social.anartist.org
> - Mail: tu_usuaria@anartist.org
> - Contraseña: ············

## 6. Audio (Funkwhale)  
Apps: [**Otter**](https://apt.izzysoft.de/fdroid/repo/com.github.apognu.otter_1000021.apk)
> Una vez instalada te pedirán que inicies sesión:
> - Host name: audio.anartist.org
> - Mail: tu_usuaria@anartist.org
> - Contraseña: ············

[Este manual se está comentando en el forum de Anartist. Si quieres añadir o modificar comenta el tema.](https://forum.anartist.org/t/docs-como-instalar-las-herramientas-de-anartist-en-tu-android/288/2)