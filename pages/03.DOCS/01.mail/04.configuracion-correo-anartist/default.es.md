---
title: 'Configuración correo Anartist'
---

## Configuración correo Anartist

En caso de querer configurar nuestro gestor de correo favorito con el correo electrónico de anartist.org, ésta es la información necesaria:

**Servidor IMAP**
```
Servidor imap: mail.anartist.org
Puerto: 143
Encriptación: STARTTLS
Autentificación: Contraseña normal
```
**Servidor POP3**
```
Servidor POP3: mail.anartist.org
Puerto: 143
Encriptación: STARTTLS
Autentificación: Contraseña normal
```
**Servidor SMTP**
```
Servidor smtp: mail.anartist.org
Puerto: 587
Seguridad: STARTTLS
Autentificación: Contraseña normal
```