---
title: DOCUMENTACIÓ
routable: false
visible: true
---

Estamos rediseñando la web, por lo que los enlaces de abajo no funcionarán. Podéis acceder a la documentación desde el menú, en el apartado DOCUMENTACIÓN

> ## Documentación Anartist
> Pequeño repositorio de documentación que iremos creando y recopilando en esta sección
> 
> GENERAL  
> [Cómo instalar las herramientas de Anartist en tu Android](https://anartist.org/documentacion/como-instalar-la-herramientas-de-anartist-en-tu-android)
> 
> MAIL  
> [Configuración correo Anartist](https://anartist.org/documentacion/configuracion-correo-anartist)
> 
> SOCIAL  
> [¿Cómo migrar de una instancia de Mastodon a otra?](https://anartist.org/documentacion/como-migrar-de-una-instancia-a-otra)
> 
> CLOUD  
> [Importar calendarios a Nextcloud](https://www.anartist.org/documentacion/importar-calendarios-nextcloud)
> 
> [Sincronizar el calendario de Nextcloud con Android](https://www.anartist.org/documentacion/sincronizar-calendario-nextcloud-android)
> 
> [Sincronizar el calendario de Nextcloud con Thunderbird](https://www.anartist.org/documentacion/sincronizar-calendario-nextcloud-thunderbird)
> 
> [Joplin, app de notas avanzada sincronizada con Nextcloud](https://www.anartist.org/documentacion/joplin-app-notas-nextcloud)