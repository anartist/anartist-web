---
title: 'Cómo preparar distribuciones GNU/Linux para creación de audio a nivel profesional'
---

[TOC]
# Ubuntu y derivadas

_Esta guía ha sido probada con éxito en Linux Mint MATE 20.04, pero debería funcionar sin problema en cualquier derivada de Ubuntu. El resultado puede variar según el entorno gráfico utilizado._

**Actualizar el sistema:**  
`sudo apt-get update && sudo apt-get upgrade `

**Añadir los repositorios Ubuntu Studio:**  
```
sudo add-apt-repository ppa:ubuntustudio-ppa/backports -y
sudo apt update
sudo apt full-upgrade
```

**Instalar Studio Controls y Carla:**  
`sudo apt install studio-controls carla`

Cuando pida permisos de tiempo real, que sí.

Después de haber instalado Studio Controls sin problema:

1. Abrir Studio Controls
2. Pulsar el botón `Fix real time permissions` en Studio Controls.
3. Reiniciar el sistema

**Instalar plugins para trabajar el sonido:**  
```
sudo apt install calf-plugins dpf-plugins-lv2 dragonfly-reverb-lv2 drumgizmo eq10q guitarix-lv2 invada-studio-plugins-lv2 lsp-plugins-lv2 lufsmeter-lv2 luftikus-lv2 swh-lv2 tal-plugins-lv2 x42-plugins zynaddsubfx-lv2
```

**Añadir el repo Ubuntu Studio Backports para tener Ardour 6:**  
_Este paso es opcional y cambiará la necesidad de ejecutarlo, cuando incluyan `Ardour 6` en los repositorios oficiales de cada distribución._
```
sudo add-apt-repository ppa:ubuntustudio-ppa/ardour-backports
sudo apt update
sudo apt upgrade
```

**Añadir los repos KXStudio:**  
_En estos repositorios encontraremos plugins que no están en los otros repositorios._
```
sudo apt-get install apt-transport-https gpgv
sudo dpkg --purge kxstudio-repos-gcc5
wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_10.0.3_all.deb
sudo dpkg -i kxstudio-repos_10.0.3_all.deb
```

**Actualizar el sistema:**  
`sudo apt-get update && sudo apt-get upgrade`

**Instalar otros plugins desde KX:**  
`sudo apt install avldrums.lv2 dexed-lv2 easyssp-lv2 geonkick helm wolf-spectrum`

**Instalar más programas (opcional y ampliable):**  
`sudo apt install musescore3 lmms`

# Cómo encontrar más plugins de sonido
Una buena técnica suele ser buscar en un **gestor de paquetes**, podría ser Synaptic en el caso de derivadas de Ubuntu, y **buscar** estos **términos**:
* jack
* lv2
* ladspa
* pro-audio

_**Nota**: **LADSPA** son el formato de plugins libres para GNU/Linux. **LV2** es su segunda versión y proviene de **L**ADSPA **V**ersion **2**._

_________
Para cualquier duda, consultar a [porrumentzio@anartist.org](mailto:porrumentzio@anartist.org).
