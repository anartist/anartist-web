---
title: MATRIX
---

## Cómo usar nuestra herramienta: MATRIX (Libertalia)

<br> </br>

Si quieres **registrarte** en el servidor Matrix de Libertalia puedes hacerlo con cualquier cliente Matrix, pero aquí te enseñaremos como hacerlo desde la aplicación de Riot:

1. Accede a https://riot.im/app
2. Dale click en «Create Account»
3. Selecciona la opción «Advanced» y añade la siguiente información:
```
- Homeserver url: https://matrix.libertalia.world
- Identity Server url: https://matrix.org
```
4. Dale click en «Next»
5. Elige tu nombre de usuario y contraseña, puedes añadir un correo electrónico de recuperación (es opcional) y teléfono (opcional también)
6. Dale click en **«Register»**
7. Una vez registrado, si has puesto un correo electrónico deberás comprobar que hayas recibido un correo para la activación de tu cuenta, en caso de no haber puesto correo electrónico tu cuenta estará activada automáticamente.

Si quieres **loggearte** en el servidor de Matrix:

1. Accede a https://riot.im/app
2. Dale click en «Change»
3. Añade la siguiente información:
```
- Homeserver url: https://matrix.libertalia.world
- Identity Server url: https://matrix.org
```
4. Dale click en «Next»
5. Pon tu nombre de usuario y contraseña
6. Dale click en **«Sign in»**
7. Ya puedes chatear a través de Matrix!
