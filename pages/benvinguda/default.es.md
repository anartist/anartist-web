---
title: benvinguda
---

### GUIA PER CREAR-TE UN COMPTE A MASTODON (O qualsevol altre eina del Fedivers)

Bones, et copio aquest missatge perquè he vist que m'has contestat interessat en treure el cap a les **xarxes socials lliures i federades (Fedivers: univers-federat)**, que per cert, avui ja suma més de 2 milions d'usuaries en tot el món. Més info sobre el fedivers: https://ca.wikipedia.org/wiki/Fedivers

Per començar, és important que sàpigues que el fedivers no és una eina, és un protocol, **igual que passa amb el correu**. Per obri-te un mail, no buscaries mail al google i et registraries al primer que et sortís. Pensaries primer, a quin proveidor de correu vull registrar-me? Gmail? Hotmail? Alguna alternativa?

Doncs una mica el mateix, el primer pas per registrar-te és **pensar desde quin servidor** (també dit instància o node) et connectaràs. Des de qualsevol podràs accedir a tot el contingut del fedivers, igual que des de qualsevol proveidor de correu pots enviar un mail a qualsevol persona. Llavors, com triar la teva instància més adequada?

Un factor important per triar-la és saber que al fedivers, hi trobem: 

· El **timeline normal**, on surt el contingut que penja la gent que segueixes (en aquest cas sense algoritmes, per odre cronològic de publicació) 

· El **timeline local**, on surt el contingut de tots els usuaris de la teva instància. Això fa que, si tries una instància adequada als teus interessos o idiomes, aquesta serà més divertida.

També cal dir que sempre estaràs a temps de canviar d'instància. En el cas de Mastodon hi ha una forma de fer-ho sense perdre seguidors ni seguits.

Aquí és on entra el projecte de Anartist. Que bàsicament som una **comunitat d'artistes** amb ganes de promocionar alternatives al Big Data i bàsicament a les empreses més grans del món a través del software lliure. Si vols saber més del nostre projecte pots anar a https://anartist.org. Nosaltres tenim una instància a Mastodon, el que passa és que per créixer de forma ordenada i mantenir l'esperit comunitari, oferim nous comptes a la gent que omple aquest formulari: anartist.org/cuenta. Tot artista de qualsevol branca està convidadíssimx!!

Si vols **una manera més ràpida** d'entrar a les xarxes socials, pots buscar altres instàncies que tinguin el registre de nous usuaris obert i instantani aquí: joinmastodon.org

Fins aquí la introducció, si tens cap dubte, **m'ofereixo a acompanyar-te** en el que calgui. Ah, i si t'has pensat que per entrar o provar les eines d'Anartist cal molta dedicació, t'has equivocat, cada persona participa com vol i pots utilitzar les eines sense implicar-te en res més.

**Portem un nou món a les nostres eines!**

PD: Un cop tingueu el compte hi podeu accedir des de l'enllaç de la vostra instància (social.anartist.org és la de anartist) o des de la app pel mobil Mastodon on haureu de posar l'enllaç de la vostra instància.