---
title: MAIL
---

**Descripción:**  
Simplemente correo electrónico.

**Enlace a la herramienta:**  
[mail.anartist.org](https://mail.anartist.org)

**Enlace a documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/mail/](https://forum.anartist.org/c/herramientas/mail/)

**Software libre:**  
 Interfaz web [Roundcube](https://roundcube.net/) y servidor [iRedMail](https://iredmail.org/)
