---
title: MAIL
---

**Descripció:**  
Simplement correu electrònic.

**Enllaç a l'eina:**  
[mail.anartist.org](https://mail.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/mail/](https://forum.anartist.org/c/herramientas/mail/)

**Programari lliure:**  
 Interfaç web [Roundcube](https://roundcube.net/) i servidor [iRedMail](https://iredmail.org/)
