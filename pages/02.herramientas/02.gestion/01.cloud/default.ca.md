---
title: CLOUD
---

**Descripció:**  
Espai d'emmagatzament de fitxers, calendari i moltes més eines per la organització interna de projectes.  
Ideal per guardar contactes, calendari, tasques, arxius... al **núvol** i compartirlos amb altres persones, per només lectura o també col·laboració.

**Enllaç a l'eina:**  
[cloud.anartist.org](https://cloud.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/cloud/](https://forum.anartist.org/c/herramientas/cloud/)

**Programari lliure:**  
[Nextcloud](https://nextcloud.com/)