---
title: CLOUD
---

**Descripción:**  
Espacio de almacenamiento de ficheros, calendario y muchas más herramientas para la organización interna de proyectos.  
Ideal para guardar contactos, calendarios, tareas, archivos... a la **nube** y compartirlos con otras perssonas, para solo lectura o también colaboración.

**Enlace a la herramienta:**  
[cloud.anartist.org](https://cloud.anartist.org)

**Enlace a la documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/cloud/](https://forum.anartist.org/c/herramientas/cloud/)

**Software libre:**  
[Nextcloud](https://nextcloud.com/)