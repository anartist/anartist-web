---
title: BLOG
---

**Descripción:**  
Blog libre y federado para compartir textos.   
Ideal para compartir **creaciones literarias** y comunicados.

**Enlace a la herramienta:**  
[blog.anartist.org](https://blog.anartist.org)

**Enlace a documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/blog/](https://forum.anartist.org/c/herramientas/blog/)

**Software libre:**  
[WriteFreely](https://writefreely.org/)
