---
title: BLOG
---

**Descripció:**  
Blog lliure i federat per compartir textos.   
Ideal per compartir **creacions literàries** i comunicats.

**Enllaç a l'eina:**  
[blog.anartist.org](https://blog.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/blog/](https://forum.anartist.org/c/herramientas/blog/)

**Programari lliure:**  
[WriteFreely](https://writefreely.org/)
