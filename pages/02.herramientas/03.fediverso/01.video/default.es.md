---
title: VIDEO
---

**Descripción:**  
Una plataforma libre y federada para compartir vídeos.  
Ideal para compartir **material audiovisual** como videotutoriales y videoclips. También soporta emisiones en directo.

**Enlace a la herramienta:**  
[video.anartist.org](https://video.anartist.org)

**Enlace a documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/video/](https://forum.anartist.org/c/herramientas/video/)

**Software libre:**  
[PeerTube](https://joinpeertube.org)