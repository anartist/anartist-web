---
title: VIDEO
---

**Descripció:**  
Una plataforma lliure i federada per compartir videos.  
Ideal per compartir **material audiovisual** com video-tutorials o video-clips. També soporta emissions en directe.

**Enllaç a l'eina:**  
[video.anartist.org](https://video.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/video/](https://forum.anartist.org/c/herramientas/video/)

**Programari lliure:**  
[PeerTube](https://joinpeertube.org)