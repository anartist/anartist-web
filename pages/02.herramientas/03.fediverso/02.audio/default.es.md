---
title: AUDIO
---

**Descripción:**  
Una plataforma libre y federada enfocada al audio.  
Ideal para compartir audios como **música** o **_podcasts_**.

**Enlace a la herramienta:**  
[audio.anartist.org](https://audio.anartist.org)

**Enlace a documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/audio/](https://forum.anartist.org/c/herramientas/audio/)

**Software libre:**  
[Funkwhale](https://funkwhale.audio)