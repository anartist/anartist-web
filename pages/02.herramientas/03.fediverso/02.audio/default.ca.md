---
title: AUDIO
---

**Descripció:**  
Una plataforma lliure i federada enfocada a l'audio.  
Ideal per compartir audios com **música** o **_podcasts_**.

**Enllaç a l'eina:**  
[audio.anartist.org](https://audio.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/audio/](https://forum.anartist.org/c/herramientas/audio/)

**Programari lliure:**  
[Funkwhale](https://funkwhale.audio)