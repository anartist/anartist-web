---
title: PICTO
---

**Descripció:**  
Una xarxa social digital lliure i federada per compartir imatges.  
Ideal per difondre **material visual** com il·lustracions, dibuixos o fotografies.

**Enllaç a l'eina:**  
[picto.anartist.org](https://picto.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/picto/](https://forum.anartist.org/c/herramientas/picto/)

**Programari lliure:**  
[Pixelfed](https://beta.joinpixelfed.org)