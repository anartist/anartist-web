---
title: PICTO
---

**Descripción:**  
Una red social digital libre y federada para compartir imágenes.  
Ideal para difundir **material visual** como ilustraciones, dibujos o fotografías.

**Enlace a la herramienta:**  
[picto.anartist.org](https://picto.anartist.org)

**Enlace a documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/picto/](https://forum.anartist.org/c/herramientas/picto/)

**Software libre:**  
[Pixelfed](https://beta.joinpixelfed.org)