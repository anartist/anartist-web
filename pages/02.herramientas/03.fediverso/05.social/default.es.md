---
title: SOCIAL
published: true
---

**Descripción:**  
Una red social digital libre y federada de microbloging.  
Ideal para **difundir** proyectos y participar de redes sociales descentralizadas, más afines a la cultura libre.

**Enlace a la herramienta:**  
[social.anartist.org](https://social.anartist.org)

**Enlace a documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del foro sobre la herramienta:**  
[forum.anartist.org/c/herramientas/social/](https://forum.anartist.org/c/herramientas/social/)

**Software libre:**  
[Mastodon](https://joinmastodon.org/).