---
title: SOCIAL
published: true
---

**Descripció:**  
Una xarxa social digital lliure i federada de microbloging.  
Ideal per **difondre** projectes i participar de les xarxes socials descentralitzades, més afines a la cultura lliure.

**Enllaç a l'eina:**  
[social.anartist.org](https://social.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Categoria del fòrum sobre l'eina:**  
[forum.anartist.org/c/herramientas/social/](https://forum.anartist.org/c/herramientas/social/)

**Programari lliure:**  
[Mastodon](https://joinmastodon.org/).