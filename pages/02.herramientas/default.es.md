---
title: HERRAMIENTAS
routable: false
---

<center>
<table>
  <tbody>
    <tr align=center>
       <td width=200 height=100><a href="https://anartist.org/forum"><img src=https://i.ibb.co/PNg9nC4/icona-discourse-negra.png></a></td>
      <td width=200 height=100><a href="https://anartist.org/mail"><img src=https://i.ibb.co/C6WTRdt/icona-correu-negra.png></a></td>

       <td width=200 height=100><a href="https://anartist.org/cloud"><img src="https://i.ibb.co/CzydTpJ/icona-nextcloud-negra.png"/></a></td>
      <td width=200 height=100><a href="https://anartist.org/social"><img src=https://i.ibb.co/7X0kwj5/icona-mastodon-negra.png></a></td>
      <td width=200 height=100><a href="https://anartist.org/pixelfed"><img src="https://i.ibb.co/Rvm5nvp/icona-pixelfed-negra.png"/></a></td>
       <td width=200 height=100><a href="https://anartist.org/funkwhale"><img src="https://i.ibb.co/5cQ5f19/icona-funkwhale-negra.png"/></a></td>
         <td width=200 height=100><a href="https://anartist.org/peertube"><img src=https://i.ibb.co/gSC628j/icona-peertube-negra.png></a></td>
    <tr align=center>
       <td width=200 height=50><b>Forum</b><br>Foro de discusión</td>
      <td width=200 height=50><b>Mail</b><br>Correo electrónico</td>

       <td width=200 height=50><b>Nextcloud</b><br>Servicio cloud</td>
      <td width=200 height=50><b>Mastodon</b><br>Fediverse microblogging</td>
      <td width=200 height=50><b>Pixelfed</b><br>Fediverse imágenes</td>
        <td width=200 height=50><b>Funkwhale</b><br>Fediverse audio</td>
       <td width=200 height=50><b>Peertube</b><br>Fediverse video</td>      
      </tr>      
  </tbody>
</table>
</center>


