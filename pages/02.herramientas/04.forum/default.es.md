---
title: FORUM
---

**Descripción:**  
Nos organizamos principalmente en el foro. Allí proponemos, debatimos y construimos la plataforma.

**Enlace a la herramienta:**  
[forum.anartist.org](https://forum.anartist.org)

**Enlace a la documentación de la herramienta:**  
[anartist.org/docs](https://anartist.org/docs)

**Software libre:**  
[Discourse](https://www.discourse.org/)