---
title: FÒRUM
---

**Descripció:**  
Ens organitzem principalment pel fòrum. Allà proposem, debatim i construim la plataforma.

**Enllaç a l'eina:**  
[forum.anartist.org](https://forum.anartist.org)

**Enllaç a la documentació de l'eina:**  
[anartist.org/docs](https://anartist.org/docs)

**Programari lliure:**  
[Discourse](https://www.discourse.org/)