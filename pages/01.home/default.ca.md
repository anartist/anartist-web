---
title: INICI
body_classes: 'title-center title-h1h2'
content:
    items: '@self.modular'
    order:
        by: ''
        dir: ''
---

<h1><center> EINES LLIURES PER A ARTISTES LLIURES </h1></center>

<center>
<table>
  <tbody>
    <tr align=center>
       <td width=200 height=100><a href="https://forum.anartist.org/"><img src=https://i.ibb.co/PNg9nC4/icona-discourse-negra.png></a></td>
      <td width=200 height=100><a href="https://mail.anartist.org/"><img src=https://i.ibb.co/C6WTRdt/icona-correu-negra.png></a></td>
       <td width=200 height=100><a href="https://cloud.anartist.org/"><img src="https://i.ibb.co/CzydTpJ/icona-nextcloud-negra.png"/></a></td>
      <td width=200 height=100><a href="https://social.anartist.org/"><img src=https://i.ibb.co/7X0kwj5/icona-mastodon-negra.png></a></td>
        <td width=200 height=100><a href="https://video.anartist.org/"><img src=https://i.ibb.co/gSC628j/icona-peertube-negra.png></a></td>
      <td width=200 height=100><a href="https://picto.anartist.org/"><img src="https://i.ibb.co/Rvm5nvp/icona-pixelfed-negra.png"/></a></td>
       <td width=200 height=100><a href="https://audio.anartist.org/"><img src="https://i.ibb.co/5cQ5f19/icona-funkwhale-negra.png"/></a></td>
       <td width=200 height=100><a href="https://blog.anartist.org/"><img src="https://lutim.anartist.org/rPC48t50.png"/></a></td>
        </tr>
    <tr align=center>
       <td width=200 height=50><b>Forum</b><br>Fòrum social</td>
      <td width=200 height=50><b>Mail</b><br>Correu electrònic</td>
       <td width=200 height=50><b>Cloud</b><br>Núvol</td>
      <td width=200 height=50><b>Social</b><br>Microblogging fedivers</td>
        <td width=200 height=50><b>Video</b><br>Vídeo fedivers</td>    
      <td width=200 height=50><b>Picto</b><br>Imatges fedivers</td>
        <td width=200 height=50><b>Audio</b><br>Àudio fedivers</td> 
        <td width=200 height=50><b>Blog</b><br>Blog fedivers</td>  
      </tr>      
  </tbody>
</table>
</center>
<br>

<h3><center> Anartist és una plataforma autogestionada per i per a artistes. </h3></center>
Els nostres objectius són:
 <body>
      <ul>
         <li> Crear una comunitat col·lectivitzant eines no comercials.</li>
         <li> Descentralizar el poder de la indústria cultural</li>
         <li> Generar espais de debat, aprenentatge i suport mutu entre artistes</li>
         <li> Construir cultura lliure i ètica per a la transformació social</li>
      </ul>
   </body> 
   
&nbsp;
<h3><center> Ens organitzem bàsicament per a dotarnos de eines anticapitalistes seguint els principis de la <a href="https://ca.wikipedia.org/wiki/Cultura_lliure">cultura lliure.</a></h3></center>
Volem emancipar-nos de les empreses privatives que controlen i vigilen a les masses. Per això, als nostres servidors tenim instal·lats (i instal·larem) les eines que necessitem per a treballar i difondre el nostre art de forma lliure, segura i sense publicitat.

<center><h2>Contribuir:</h2></center>
<center>
El manteniment dels nostres servidors té un cost. Si vols col·laborar en finançar les serveis d'Anartist pots aportar a través del link: <a href=https://anartist.org/es/comunidad/contribuir>Contribuir</a>
</center>

<a rel="me" href="https://social.anartist.org/@anartist">Mastodon</a>