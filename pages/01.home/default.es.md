---
title: INICIO
body_classes: 'title-center title-h1h2'
content:
    items: '@self.modular'
    order:
        by: ''
        dir: ''
---

<h1><center> HERRAMIENTAS LIBRES PARA ARTISTAS LIBRES </h1></center>

<center>
<table>
  <tbody>
    <tr align=center>
       <td width=200 height=100><a href="https://forum.anartist.org/"><img src=https://i.ibb.co/PNg9nC4/icona-discourse-negra.png></a></td>
      <td width=200 height=100><a href="https://mail.anartist.org/"><img src=https://i.ibb.co/C6WTRdt/icona-correu-negra.png></a></td>
       <td width=200 height=100><a href="https://cloud.anartist.org/"><img src="https://i.ibb.co/CzydTpJ/icona-nextcloud-negra.png"/></a></td>
      <td width=200 height=100><a href="https://social.anartist.org/"><img src=https://i.ibb.co/7X0kwj5/icona-mastodon-negra.png></a></td>
        <td width=200 height=100><a href="https://video.anartist.org/"><img src=https://i.ibb.co/gSC628j/icona-peertube-negra.png></a></td>
      <td width=200 height=100><a href="https://picto.anartist.org/"><img src="https://i.ibb.co/Rvm5nvp/icona-pixelfed-negra.png"/></a></td>
       <td width=200 height=100><a href="https://audio.anartist.org/"><img src="https://i.ibb.co/5cQ5f19/icona-funkwhale-negra.png"/></a></td>
       <td width=200 height=100><a href="https://blog.anartist.org/"><img src="https://lutim.anartist.org/rPC48t50.png"/></a></td>
        </tr>
    <tr align=center>
       <td width=200 height=50><b>Forum</b><br>Foro social</td>
      <td width=200 height=50><b>Mail</b><br>Correo electrónico</td>
       <td width=200 height=50><b>Cloud</b><br>Nube</td>
      <td width=200 height=50><b>Social</b><br>Microblogging fediverso</td>
        <td width=200 height=50><b>Video</b><br>Video fediverso</td>    
      <td width=200 height=50><b>Picto</b><br>Imágenes fediverso</td>
        <td width=200 height=50><b>Audio</b><br>Audio fediverso</td> 
        <td width=200 height=50><b>Blog</b><br>Blog fediverso</td>  
      </tr>      
  </tbody>
</table>
</center>
<br>

<h3><center> Anartist es una plataforma autogestionada por y para artistas. </h3></center>
Nuestros objetivos son:
 <body>
      <ul>
         <li> Crear una comunidad colectivizando herramientas no comerciales </li>
         <li> Descentralizar el poder de la industria cultural</li>
         <li> Generar espacios de debate, aprendizaje y apoyo mutuo entre artistas</li>
         <li> Construir cultura libre y ética para la transformación social</li>
      </ul>
   </body> 
   
&nbsp;
<h3><center> Nos organizamos básicamente para dotarnos de herramientas anticapitalistas siguiendo los principios de la <a href="https://es.wikipedia.org/wiki/Cultura_libre">cultura libre.</a></h3></center>
Queremos emanciparnos de las empreses privativas que controlan y vigilan a las masas. Por eso en nuestros servidores tenemos instaladas (e instalaremos) las herramientas que necesitamos para trabajar y difundir nuestro arte de forma libre, segura y sin publicidad.  


<center><h2>Contribuir:</h2></center>
<center>
El mantenimiento de nuestros servidores tiene un coste. Si quieres colaborar en financiar los servicios de Anartist puedes aportar algo a través del link: <a href=https://anartist.org/es/comunidad/contribuir>Contribuir</a>
</center>

<a rel="me" href="https://social.anartist.org/@anartist">Mastodon</a>